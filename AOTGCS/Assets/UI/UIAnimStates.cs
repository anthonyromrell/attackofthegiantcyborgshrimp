﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIAnimStates : MonoBehaviour
{
    public static UnityAction CanPlay;
    public Animator guiAnim;

    public Text startText;

    public void OnChange(string _text)
    {
        startText.text = _text;
    }

    public void OnCanPlay()
    {
    }

    public void OnEnd()
    {
        //NOT READY
        startText.text = "Game Over";
        startText.fontSize = 150;
    }

    private IEnumerator EndGUI()
    {
        yield return new WaitForSeconds(1f);
        guiAnim.SetBool("EndGUI", true);
    }

    private void EndThisGUI()
    {
        StartCoroutine(EndGUI());
    }
}