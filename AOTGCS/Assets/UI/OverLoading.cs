﻿using UnityEngine;

public class OverLoading : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        CoolBeans("Cool");
        CoolBeans(2);
        CoolBeans();
    }

    private void CoolBeans(string _s)
    {
        Debug.Log(_s);
    }

    private void CoolBeans(int _i)
    {
        Debug.Log(_i);
    }

    private void CoolBeans()
    {
        Debug.Log("Nothing");
    }
}