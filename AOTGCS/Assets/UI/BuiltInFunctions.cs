﻿using UnityEngine;

public class BuiltInFunctions : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        Debug.Log("Runs once");
    }

    // Update is called once per frame
    private void Update()
    {
        Debug.Log("Runs every frame, at the end of the frame");
    }

    private void FixedUpdate()
    {
        Debug.Log("Runs every frame, at the start of the frame");
    }

    private void OnMouseOver()
    {
        Debug.Log("Runs on a mouse over");
    }

    private void OnDisable()
    {
        Debug.Log("Runs when disabled");
    }

    private void OnEnable()
    {
        Debug.Log("Runs when enabled");
    }
}