﻿using UnityEngine;

public class ButtonOver : MonoBehaviour
{
    public SpriteRenderer mySPR;

    private void OnMouseOver()
    {
        mySPR.color = Color.red;
    }

    private void OnMouseExit()
    {
        mySPR.color = Color.white;
    }
}