﻿using System.Collections;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class UICounter : MonoBehaviour
{
    public Animator Anims;
    public Text AnimsText;
    public IntData NumberCollected;
    public GameAction PowerDown;

    public GameAction PowerUp;
    private Text uiText;

    private void Start()
    {
        uiText = GetComponent<Text>();
        PowerUp.raise += UpdateValueHandler;
        PowerDown.raise += UpdateValueHandler;
        uiText.text = NumberCollected.value.ToString("N0", CultureInfo.CurrentCulture);
    }

    private void UpdateValueHandler(object obj)
    {
        var value = (int) obj;
        StartCoroutine(value > 0 ? AddValue(value) : SubtractValue(value));

        AnimsText.text = "+" + value.ToString("N0", CultureInfo.CurrentCulture);
        Anims.SetTrigger("Collect");
    }

    private IEnumerator AddValue(int value)
    {
        var tempValue = NumberCollected.value + value;
        while (NumberCollected.value+1 < tempValue)
        {
            NumberCollected.value++;
            uiText.text = NumberCollected.value.ToString("N0", CultureInfo.CurrentCulture);
            yield return new WaitForFixedUpdate();
        }
    }

    private IEnumerator SubtractValue(int value)
    {
        var tempValue = NumberCollected.value - value;
        if (tempValue < 0)
        {
            var fixValue = tempValue;
            tempValue = NumberCollected.value - value - fixValue;
        }

        while (NumberCollected.value > tempValue)
        {
            NumberCollected.value--;
            uiText.text = NumberCollected.value.ToString("N0", CultureInfo.CurrentCulture);
            yield return new WaitForFixedUpdate();
        }
    }
}