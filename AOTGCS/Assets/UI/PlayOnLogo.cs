﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayOnLogo : MonoBehaviour
{
    public string levelToLoad;

    public void OnPlay()
    {
        var anim = GetComponent<Animator>();
        anim.SetTrigger("Exit");
    }

    public void OnLoadNextLevel()
    {
        SceneManager.LoadScene(levelToLoad);
    }
}