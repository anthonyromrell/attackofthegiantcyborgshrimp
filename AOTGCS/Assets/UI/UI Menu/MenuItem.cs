﻿using UnityEngine;

[CreateAssetMenu]
public class MenuItem : ScriptableObject
{
    public Color itemColor = Color.red;
    public string itemMessage = "Hello World";
    
    public void ItemWork()
    {
        Debug.Log(itemMessage);
    }

}
