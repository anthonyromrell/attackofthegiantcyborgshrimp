﻿using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{

    public MenuButton ButtonToInstance;
    public List<MenuItem> MenuItems;
    
    void Start()
    {
        foreach (var item in MenuItems)
        {
            var newMenuButton = Instantiate(ButtonToInstance, transform);
            newMenuButton.img.color = item.itemColor;
            newMenuButton.Label.text = item.name;
            newMenuButton.btn.onClick.AddListener(item.ItemWork);
        }
    }
}