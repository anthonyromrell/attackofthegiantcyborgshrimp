﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image), typeof(Button), typeof(RectTransform))]
public class MenuButton : MonoBehaviour
{
    [HideInInspector] public Image img;
    [HideInInspector] public Button btn;
    [HideInInspector] public RectTransform rct;
    public Text Label;
    
    private void Awake()
    {
        img = GetComponent<Image>();
        btn = GetComponent<Button>();
        rct = GetComponent<RectTransform>();
    }
}