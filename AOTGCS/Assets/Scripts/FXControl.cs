﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class FXControl : MonoBehaviour
{
    public ParticleSystem pSystem;
    public GameObject gObj;
    public float EndTime = 0.25f;

    private void Start()
    {
        End();
    }

    public void Run () {
        
        gObj.SetActive(true);
        pSystem.Play();
        Invoke(nameof(End), EndTime);
    
    }
    
    public void End()
    {
        pSystem.Stop();
        Invoke(nameof(TurnOff), EndTime);
    }

    public void TurnOff()
    {
        gObj.SetActive(false);
    }
    
}
