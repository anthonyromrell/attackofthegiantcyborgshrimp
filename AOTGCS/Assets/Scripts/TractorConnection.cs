﻿using UnityEngine;

public class TractorConnection : MonoBehaviour
{
    public Rigidbody newRigidBody;
    public Rigidbody oldRigidBody;

    public SpringJoint thisSpringJoint;

    private void OnTriggerEnter(Collider _collider)
    {
        Debug.Log(_collider.GetComponent<Rigidbody>());
        thisSpringJoint.connectedBody = newRigidBody;
    }

    private void OnMouseDown()
    {
        Debug.Log("CLICK");
        thisSpringJoint.connectedBody = oldRigidBody;
    }

    private void Update()
    {
        GetComponent<Rigidbody>().AddForce(0, 0, -10);
    }
}