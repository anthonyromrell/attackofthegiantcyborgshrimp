﻿using System.Collections;
using UnityEngine;

public class TransformTargetDirector : MonoBehaviour
{
    public float smoothing = 1.0F; 
    public Vector3 startPosition; 
    public GameAction updateEnemyTargetEvent;
    
    private void StartToNextPosition(object playerTarget)
    {
        var newTarget = playerTarget as Transform;
        StartCoroutine(SetNewPosition(newTarget)); 
    }

    private void OnDisable()
    {
        updateEnemyTargetEvent.raise -= StartToNextPosition; 
    }

    private void OnEnable()
    {
        updateEnemyTargetEvent.raise += StartToNextPosition; 
        transform.localPosition = startPosition;
    }

    private IEnumerator SetNewPosition(Transform playerTarget) 
    {
        while (Vector3.Distance(transform.position, playerTarget.position) > 0.1f)
        {
            var step = smoothing * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, playerTarget.position, step);
            yield return null;
        }
    }
}