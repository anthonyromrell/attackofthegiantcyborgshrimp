﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour
{
    //TODO Fix this
    public Enemy enemyData;
    public PoolControl poolControlObj;
    private Animator anims;
    private float health = 1;
    private float velocity;

    public EnemyController(float velocity)
    {
        this.velocity = velocity;
    }

    private void Awake()
    {
        anims = GetComponentInChildren<Animator>();
        gameObject.SetActive(true);
        Invoke(nameof(Deactivate), 1.5f);
        anims.SetFloat($"Swim", 0);
    }

    private void OnEnable()
    {
        anims.SetLayerWeight(2, 0);
        health = enemyData.HealthRenewalLevel;
        anims.SetLayerWeight(2, 0);
        gameObject.SetActive(true);
    }

    public void StartEnemyMove()
    {
        anims.SetFloat($"Swim", velocity);
    }

    private void Deactivate()
    {
        anims.SetBool($"Explode", false);
        gameObject.SetActive(false);
        anims.SetLayerWeight(2, 0f);
    }

    private IEnumerator PlayDamageAnim()
    {
        var i = Random.Range(0.5f, 1);
        anims.SetLayerWeight(2, i);
        anims.SetBool($"Damage", true);
        yield return new WaitForSeconds(0.2f);
        anims.SetLayerWeight(2, 0);
        anims.SetBool($"Damage", false);
    }

    private void LowerHealth(Collider c)
    {
        StartCoroutine(PlayDamageAnim());
        
        if (!(health <= 0)) return;
        enemyData.DeathFX.SetActive(true);
        Invoke(nameof(Deactivate), 1.5f);
    }

    private void OnTriggerEnter(Collider c)
    {
        LowerHealth(c);
    }

    private void OnDisable()
    {
        poolControlObj.AddObj(gameObject);
        
    }
}