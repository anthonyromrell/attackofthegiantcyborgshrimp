﻿using UnityEngine;

[CreateAssetMenu(fileName = "Enemy")]
public class Enemy : ScriptableObject
{
	public float powerLevel = 1;
	public float HealthRenewalLevel = 1;
	public float TurnDelay = 1;

	public GameObject DeathFX;
	public GameObject AmmoArt;
}
